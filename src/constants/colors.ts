export enum Colors {
  white = '#fff',
  lightBlue = '#6aa4f0',
  navyBlue = '#4584fe',
  map = '#f1f8ff',
  mapMarker = '#ff8600',
  skyBlue = '#88c3ff',
  mainFont = '#949494',
}

export enum URLS {
  baseUrl = 'https://api.weatherapi.com/v1/',
  currentWeather = 'current.json',
  weatherForecast = 'forecast.json',
}

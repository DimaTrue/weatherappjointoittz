import axios, { AxiosRequestConfig } from 'axios';

import { URLS, WEATHER_API_KEY } from '../constants';

export const getWeatherByCoordinates = (
  longitude: number,
  latitude: number,
) => {
  const options: AxiosRequestConfig = {
    method: 'GET',
    url: `${URLS.baseUrl}${URLS.currentWeather}`,
    params: {
      key: WEATHER_API_KEY,
      q: `${latitude},${longitude}`,
      aqi: 'no',
    },
  };

  return axios.request(options);
};

export const getWeatherForecast = (place: string) => {
  const options: AxiosRequestConfig = {
    method: 'GET',
    url: `${URLS.baseUrl}${URLS.weatherForecast}`,
    params: {
      key: WEATHER_API_KEY,
      q: `${place}`,
      aqi: 'no',
      days: 7,
    },
  };

  return axios.request(options);
};

import React, { useEffect, useState } from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  FlatList,
  Keyboard,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Card } from 'react-native-paper';

import { getWeatherForecastInit } from '../../store';
import {
  rootState,
  SearchScreenProps,
  IForecastDay,
  RenderItemForecastDay,
} from '../../types';
import { ForecastDayComponent } from '../../components';

import styles from './styles';
import { Colors } from '../../constants';

export const SearchScreen = ({ navigation }: SearchScreenProps) => {
  const dispatch = useDispatch();
  const country = useSelector((state: rootState) => state.map.currentCountry);
  const placeName = useSelector(
    (state: rootState) => state.map.currentPlaceName,
  );
  const forecastDays = useSelector(
    (state: rootState) => state.map.forecastDays,
  );
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      const requestPlace = `${placeName},${country}`;
      if (requestPlace.length > 1) {
        setInputValue(requestPlace);
        dispatch(getWeatherForecastInit(requestPlace));
      } else {
        setInputValue('');
      }
    });
    return unsubscribe;
  }, [navigation, dispatch, country, placeName]);

  const handleSearch = (text: string) => {
    if (text.length) {
      dispatch(getWeatherForecastInit(text));
    }
    Keyboard.dismiss();
  };

  const keyExtractor = (item: IForecastDay) => {
    return item.date;
  };

  const renderItem = ({ item }: RenderItemForecastDay) => {
    return <ForecastDayComponent {...item} />;
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchBlock}>
        <Card elevation={24}>
          <TextInput
            style={styles.input}
            value={inputValue}
            onChangeText={(text: string) => setInputValue(text)}
          />
        </Card>
        <Card style={styles.searchBtnCard}>
          <TouchableOpacity
            style={styles.searchBtn}
            onPress={() => handleSearch(inputValue)}>
            <Icon name="search" size={35} color={Colors.lightBlue} />
          </TouchableOpacity>
        </Card>
      </View>
      <View style={styles.forecastBlock}>
        <FlatList
          data={forecastDays}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      </View>
    </View>
  );
};

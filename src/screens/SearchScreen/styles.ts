import { StyleSheet, Dimensions } from 'react-native';

import { Colors } from '../../constants';

const { width } = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: Colors.white,
  },
  searchBlock: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 7,
  },
  input: {
    width: width * 0.7,
    fontSize: 18,
    color: Colors.mainFont,
  },
  searchBtn: {
    width: width * 0.2,
    borderColor: Colors.navyBlue,
    borderWidth: 1,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  searchBtnCard: {
    borderRadius: 50,
    elevation: 10,
  },
  forecastBlock: {
    alignItems: 'center',
  },
});

export default styles;

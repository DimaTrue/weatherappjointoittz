import { StyleSheet, Dimensions } from 'react-native';
import { Colors } from '../../constants';

const { height } = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  title: {
    flex: 1,
    paddingTop: height / 20,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    color: Colors.lightBlue,
  },
});

export default styles;

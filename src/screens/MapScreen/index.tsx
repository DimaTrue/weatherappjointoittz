import React from 'react';
import { View, Text } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { useDispatch } from 'react-redux';

import { CustomMarker } from '../../components';

import styles from './styles';
import { mapStyle } from './mapStyle';
import { RoutesConstants } from '../../constants';
import { MapScreenProps } from '../../types';
import { getCurrentPlace } from '../../store';

export const MapScreen = ({ navigation }: MapScreenProps) => {
  const dispatch = useDispatch();

  return (
    <View style={styles.container}>
      <MapView
        onLongPress={e => dispatch(getCurrentPlace(e.nativeEvent.coordinate))}
        onCalloutPress={() => navigation.navigate(RoutesConstants.search)}
        customMapStyle={mapStyle}
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        region={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}>
        <CustomMarker />
      </MapView>
      <Text style={styles.title}>Location</Text>
    </View>
  );
};

import { StyleSheet } from 'react-native';
import { Colors } from '../../constants';

const styles = StyleSheet.create({
  textCallout: {
    fontSize: 18,
    color: Colors.mainFont,
  },
  circle: {
    backgroundColor: 'transparent',
    height: 37,
    width: 37,
    borderRadius: 50,
    borderWidth: 11,
    borderColor: Colors.mapMarker,
    marginBottom: -4,
  },
  callOutBox: {
    flex: 1,
    justifyContent: 'space-around',
    width: 200,
    height: 90,
    borderRadius: 5,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: Colors.white,
  },
});

export default styles;

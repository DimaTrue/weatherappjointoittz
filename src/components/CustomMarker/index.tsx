import React from 'react';
import { View, Text } from 'react-native';
import { Marker, Callout } from 'react-native-maps';
import Svg, { Polyline } from 'react-native-svg';
import { useSelector } from 'react-redux';

import { Colors } from '../../constants';
import styles from './styles';
import { rootState } from '../../types';

export const CustomMarker = () => {
  const coordinates = useSelector(
    (state: rootState) => state.map.currentCoordinates,
  );
  const country = useSelector((state: rootState) => state.map.currentCountry);
  const placeName = useSelector(
    (state: rootState) => state.map.currentPlaceName,
  );
  const temperature = useSelector(
    (state: rootState) => state.map.currentTemperature,
  );
  const errorMessage = useSelector(
    (state: rootState) => state.map.currentWeatherError,
  );

  if (coordinates.longitude && coordinates.latitude) {
    return (
      <Marker
        coordinate={{
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
        }}
        calloutAnchor={{ x: 2.5, y: -0.1 }}>
        <View>
          <View style={styles.circle} />
          <View>
            <Svg width="40" height={20}>
              <Polyline
                points="7,0 19,11 30,0"
                fill={Colors.mapMarker}
                stroke={Colors.mapMarker}
                strokeWidth="1"
              />
            </Svg>
          </View>
        </View>
        <Callout tooltip>
          <View style={styles.callOutBox}>
            {!!errorMessage.length && (
              <Text style={styles.textCallout}>{errorMessage}</Text>
            )}
            {!!placeName.length && !!country.length && (
              <Text
                style={styles.textCallout}>{`${placeName}, ${country}`}</Text>
            )}
            {!(typeof temperature === 'undefined') && (
              <Text style={styles.textCallout}>
                {temperature > 0 && '+'}
                {`${temperature}°`}
              </Text>
            )}
          </View>
        </Callout>
      </Marker>
    );
  } else {
    return null;
  }
};

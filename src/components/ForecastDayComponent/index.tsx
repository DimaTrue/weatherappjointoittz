import React from 'react';
import { View, Text } from 'react-native';
import { Card } from 'react-native-paper';
import moment from 'moment';

import styles from './styles';
import { IForecastDay } from '../../types';

export const ForecastDayComponent = ({ date, temperature }: IForecastDay) => {
  let dateValue = moment(date, 'YYYY-MM-DD HH:mm:ss');

  return (
    <Card style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.dateText}>{dateValue.format('dddd')}</Text>
        <Text style={styles.temperatureText}>
          {temperature > 0 && '+'}
          {`${temperature}°`}
        </Text>
      </View>
    </Card>
  );
};

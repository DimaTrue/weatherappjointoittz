import { StyleSheet, Dimensions } from 'react-native';
import { Colors } from '../../constants';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.skyBlue,
    marginTop: height * 0.04,
    elevation: 10,
    borderRadius: 10,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.skyBlue,
    width: width * 0.7,
    height: height * 0.07,
    borderRadius: 10,
  },
  dateText: {
    color: Colors.white,
    paddingLeft: width * 0.04,
  },
  temperatureText: {
    color: Colors.white,
    fontSize: 18,
    paddingRight: width * 0.1,
  },
});

export default styles;

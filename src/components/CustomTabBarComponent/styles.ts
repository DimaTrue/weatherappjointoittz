import { StyleSheet, Dimensions } from 'react-native';
import { Colors } from '../../constants';

const { height } = Dimensions.get('screen');

const styles = StyleSheet.create({
  tabBar: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
  },
  btn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.lightBlue,
    borderRadius: 50,
    height: height / 15,
  },
  focusedBtn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.navyBlue,
    borderRadius: 50,
    height: height / 15,
  },
  text: {
    color: Colors.white,
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  focusedText: {
    color: Colors.white,
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
});

export default styles;

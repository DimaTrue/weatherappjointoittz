import { all } from 'redux-saga/effects';

import MapSagas from './map';

export default function* rootSaga() {
  yield all([MapSagas()]);
}

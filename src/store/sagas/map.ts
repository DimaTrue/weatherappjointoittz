import { put, takeLatest, all, call } from 'redux-saga/effects';

import {
  getCurrentWeatherSucces,
  getCurrentWeatherFailure,
  getWeatherForecastSuccess,
  getWeatherForecastFailure,
} from '../reducers';
import {
  IgetCurrentPlaceAction,
  IgetWeatherForeCastInit,
  MapActionTypes,
} from '../../types';
import { getWeatherByCoordinates, getWeatherForecast } from '../../api-client';
import { SagaIterator } from 'redux-saga';

export function* watchGetWeatherByCoordinates(): SagaIterator {
  yield takeLatest(
    MapActionTypes.GET_CURRENT_PLACE,
    getWeatherByCoordinatesSaga,
  );
}

export function* getWeatherByCoordinatesSaga({
  payload,
}: IgetCurrentPlaceAction): SagaIterator {
  try {
    const result = yield call(
      getWeatherByCoordinates,
      payload.longitude,
      payload.latitude,
    );
    let temperature;
    if (result?.data?.current?.temp_c) {
      temperature = Math.round(result?.data?.current?.temp_c);
    }
    yield put(
      getCurrentWeatherSucces({
        currentCountry: result?.data?.location?.country,
        currentPlaceName: result?.data?.location?.name,
        currentTemperature: temperature,
      }),
    );
  } catch (error) {
    yield put(getCurrentWeatherFailure(error.message));
    console.warn(error);
  }
}

export function* watchGetWeatherForecast(): SagaIterator {
  yield takeLatest(
    MapActionTypes.GET_WEATHER_FORECAST_INIT,
    getWeatherForecastSaga,
  );
}

export function* getWeatherForecastSaga({
  payload,
}: IgetWeatherForeCastInit): SagaIterator {
  try {
    const result = yield call(getWeatherForecast, payload);
    const forecastDays = result.data.forecast.forecastday.map((el: any) => {
      let temperature = Math.round(el?.day?.avgtemp_c);
      return {
        date: el.date,
        temperature,
      };
    });
    yield put(getWeatherForecastSuccess(forecastDays));
  } catch (error) {
    yield put(getWeatherForecastFailure(error.message));
    console.warn(error);
  }
}

export default function* MapSagas() {
  yield all([watchGetWeatherByCoordinates(), watchGetWeatherForecast()]);
}

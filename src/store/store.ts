import { createStore, applyMiddleware, compose, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import { rootReducer } from './reducers';
import rootSaga from './sagas';
import { rootState } from '../types';

const composeEnhancers: typeof composeWithDevTools =
  process.env.NODE_ENV !== 'production' ? composeWithDevTools : compose;

const sagaMiddleware = createSagaMiddleware();

export const store: Store<rootState> = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(rootSaga);

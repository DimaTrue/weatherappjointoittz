import {
  MapActionTypes,
  MapActions,
  IcurrentCoordinates,
  IMapState,
  IweatherResponseProps,
  IForecastDay,
} from '../../types';

const initialState: IMapState = {
  currentCoordinates: {
    longitude: 0,
    latitude: 0,
  },
  currentCountry: '',
  currentPlaceName: '',
  currentTemperature: undefined,
  currentWeatherError: '',
  forecastDays: [],
  weatherForecastError: '',
};

export default function (
  state: IMapState = initialState,
  action: MapActions,
): IMapState {
  switch (action.type) {
    case MapActionTypes.GET_CURRENT_PLACE:
      return { ...state, currentCoordinates: action.payload };

    case MapActionTypes.GET_CURRENT_WEATHER_SUCCESS:
      const { currentCountry, currentPlaceName, currentTemperature } =
        action.payload;
      return {
        ...state,
        currentCountry,
        currentPlaceName,
        currentTemperature,
        currentWeatherError: '',
      };

    case MapActionTypes.GET_CURRENT_WEATHER_FAILURE:
      return {
        ...state,
        currentWeatherError: action.payload,
        currentCountry: '',
        currentPlaceName: '',
        currentTemperature: undefined,
      };

    case MapActionTypes.GET_WEATHER_FORECAST_INIT:
      return { ...state };

    case MapActionTypes.GET_WEATHER_FORECAST_SUCCESS:
      return {
        ...state,
        forecastDays: action.payload,
        weatherForecastError: '',
      };

    case MapActionTypes.GET_WEATHER_FORECAST_FAILURE:
      return {
        ...state,
        forecastDays: [],
        weatherForecastError: action.payload,
      };

    default:
      return state;
  }
}

export const getCurrentPlace = (
  coordinates: IcurrentCoordinates,
): MapActions => ({
  type: MapActionTypes.GET_CURRENT_PLACE,
  payload: coordinates,
});

export const getCurrentWeatherSucces = (
  payload: IweatherResponseProps,
): MapActions => ({
  type: MapActionTypes.GET_CURRENT_WEATHER_SUCCESS,
  payload,
});

export const getCurrentWeatherFailure = (error: string): MapActions => ({
  type: MapActionTypes.GET_CURRENT_WEATHER_FAILURE,
  payload: error,
});

export const getWeatherForecastInit = (payload: string) => ({
  type: MapActionTypes.GET_WEATHER_FORECAST_INIT,
  payload,
});

export const getWeatherForecastSuccess = (payload: IForecastDay[]) => ({
  type: MapActionTypes.GET_WEATHER_FORECAST_SUCCESS,
  payload,
});

export const getWeatherForecastFailure = (error: string) => ({
  type: MapActionTypes.GET_WEATHER_FORECAST_FAILURE,
  payload: error,
});

import { TabNavigationState } from '@react-navigation/routers';
import { ParamListBase } from '@react-navigation/native';
import {
  BottomTabDescriptorMap,
  BottomTabNavigationEventMap,
} from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import { NavigationHelpers } from '@react-navigation/core/lib/typescript/src/types';

import { RoutesConstants } from './constants';
import { rootReducer } from './store';

export type NavigationType = NavigationHelpers<
  ParamListBase,
  BottomTabNavigationEventMap
>;

export type WeatherTabNavigatorParamsList = {
  [RoutesConstants.map]: undefined;
  [RoutesConstants.search]: undefined;
};

export type CustomTabBarComponentProps = {
  state: TabNavigationState<ParamListBase>;
  descriptors: BottomTabDescriptorMap;
  navigation: NavigationType;
};

export type MapScreenProps = {
  navigation: NavigationType;
};

export type SearchScreenProps = {
  navigation: any;
};

export interface IcurrentCoordinates {
  longitude: number;
  latitude: number;
}

export interface IweatherResponseProps {
  currentCountry: string;
  currentPlaceName: string;
  currentTemperature: number | undefined;
}

export interface IForecastDay {
  date: string;
  temperature: number;
}

export enum MapActionTypes {
  GET_CURRENT_PLACE = 'GET_CURRENT_PLACE',
  GET_CURRENT_WEATHER_SUCCESS = 'GET_CURRENT_WEATHER_SUCCESS',
  GET_CURRENT_WEATHER_FAILURE = 'GET_CURRENT_WEATHER_FAILURE',
  GET_WEATHER_FORECAST_INIT = 'GET_WEATHER_FORECAST_INIT',
  GET_WEATHER_FORECAST_SUCCESS = 'GET_WEATHER_FORECAST_SUCCESS',
  GET_WEATHER_FORECAST_FAILURE = 'GET_WEATHER_FORECAST_FAILURE',
}

export interface IgetCurrentPlaceAction {
  type: MapActionTypes.GET_CURRENT_PLACE;
  payload: IcurrentCoordinates;
}

export interface IgetCurrentWeatherSuccesAction {
  type: MapActionTypes.GET_CURRENT_WEATHER_SUCCESS;
  payload: IweatherResponseProps;
}

export interface IgetCurrentWeatherFailureAction {
  type: MapActionTypes.GET_CURRENT_WEATHER_FAILURE;
  payload: string;
}

export interface IgetWeatherForeCastInit {
  type: MapActionTypes.GET_WEATHER_FORECAST_INIT;
  payload: string;
}

export interface IgetWeatherForeCastSuccess {
  type: MapActionTypes.GET_WEATHER_FORECAST_SUCCESS;
  payload: IForecastDay[];
}

export interface IgetWeatherForeCastFailure {
  type: MapActionTypes.GET_WEATHER_FORECAST_FAILURE;
  payload: string;
}

export type MapActions =
  | IgetCurrentPlaceAction
  | IgetCurrentWeatherSuccesAction
  | IgetCurrentWeatherFailureAction
  | IgetWeatherForeCastInit
  | IgetWeatherForeCastSuccess
  | IgetWeatherForeCastFailure;

export interface IMapState {
  readonly currentCoordinates: IcurrentCoordinates;
  readonly currentCountry: string;
  readonly currentPlaceName: string;
  readonly currentTemperature: number | undefined;
  readonly currentWeatherError: string;
  readonly forecastDays: IForecastDay[];
  readonly weatherForecastError: string;
}

export type rootState = ReturnType<typeof rootReducer>;

export type SagaCoordinatesPayload = {
  payload: IcurrentCoordinates;
};

export type RenderItemForecastDay = {
  item: IForecastDay;
};

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import WeatherTabNavigator from './WeatherTabNavigator';

const MainNavigator = () => {
  return (
    <NavigationContainer>
      <WeatherTabNavigator />
    </NavigationContainer>
  );
};

export default MainNavigator;

import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { MapScreen, SearchScreen } from '../screens';
import { CustomTabBarComponent } from '../components';
import { RoutesConstants } from '../constants';
import { WeatherTabNavigatorParamsList } from '../types';

const Tab = createBottomTabNavigator<WeatherTabNavigatorParamsList>();

const WeatherTabNavigator = () => {
  return (
    <Tab.Navigator
      tabBar={props => <CustomTabBarComponent {...props} />}
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
      }}>
      <Tab.Screen name={RoutesConstants.map} component={MapScreen} />
      <Tab.Screen name={RoutesConstants.search} component={SearchScreen} />
    </Tab.Navigator>
  );
};

export default WeatherTabNavigator;

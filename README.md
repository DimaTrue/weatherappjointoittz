# Weater App

Weather api - https://www.weatherapi.com/docs/

1. Not implemented elevation in callout in marker in react-native-maps
   issue - https://github.com/react-native-maps/react-native-maps/issues/3006

2. Only three days forecast for free - https://www.weatherapi.com/pricing.aspx

3. Google-places-autocomplete was not implemented because my trial period expired in google cloud console.
